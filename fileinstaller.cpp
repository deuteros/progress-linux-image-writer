#include "fileinstaller.h"

FileInstaller::FileInstaller(QObject *parent) : QObject(parent)
{
    connect(&m_command,SIGNAL(finished(int)),this,SLOT(onFinished(int)));
    connect(&m_command,&QProcess::errorOccurred,this,&FileInstaller::onErrorOccurred);
    connect(&m_command,&QProcess::readyReadStandardError,this,&FileInstaller::onReadyReadStandardError);
    connect(&m_command,&QProcess::readyReadStandardOutput,this,&FileInstaller::onReadyReadStandardOutput);
}

void FileInstaller::callCommand( QString command, QStringList args){
    m_command.start(command, args);
}


void FileInstaller::mkfsext3(QString device){
    QString partition("/dev/"+device+"3");
    emit message("<b>Formatting partition "+ partition + "</b");
    callCommand("mkfs.ext3",QStringList() << "-q" << partition );

}

void FileInstaller::mount(QString device){
    QString partition("/dev/"+device+"3");
    emit message("<b>Mounting "+ partition + "</b");
    callCommand("mount",QStringList() << partition << "/mnt");

}

void FileInstaller::umount(QString device){
    QString partition("/dev/"+device+"3");
    emit message("<b>Disattaching "+ partition + "</b");
    callCommand("umount",QStringList() << partition);
}

void FileInstaller::sync(){
    emit message("<b>Syncing files</b");
    callCommand("sync",QStringList());
}

void FileInstaller::extract(QString file, QString targetDir){
    emit message("<b>Extracting rootfs from " + QUrl(file).path()  + "</b");
    callCommand("tar",QStringList() << "-xvpf" << QUrl(file).path() << "-C" << targetDir <<"--same-owner" << "--preserve-order");
}

void FileInstaller::cd(QString dir){
    qDebug() << "Changing directory to " << dir;
    emit message("<b>Changing directory to " + dir  + "</b");
    callCommand("cd",QStringList() << dir);
    qDebug() << "1";
}

void FileInstaller::copyFile(QString file, QString targetFile){
    emit message("<b>Copying " +QUrl(file).path()+" to " +targetFile + "</b>");
    QDir dir= QDir(QFileInfo(targetFile).dir());
    emit message(dir.path());
    if (!dir.exists()){
        dir.mkpath(dir.path());
    }
    callCommand("cp", QStringList() << QUrl(file).path() << targetFile);
}

void FileInstaller::onFinished(int exitCode){
    if (exitCode==0){
        QString data=m_command.readAll();
        emit message(QString("Done! <pre>"+data.replace(QRegExp("[\\n\\r]"),"<br>")+"</pre>"));
        emit finished();
    }else{
        emit errorMessage("An error occurred: Error code is" + QString::number(m_command.error()));
    }
}

void FileInstaller::onErrorOccurred(QProcess::ProcessError err){
    emit errorMessage("An error occurred: Error code is " + QString::number(err));
}

void FileInstaller::onReadyReadStandardError(){
    emit errorMessage(QString(m_command.readAllStandardError()).replace(QRegExp("[\\n\\r]"),"<br>"));
}

void FileInstaller::onReadyReadStandardOutput(){
    emit message(QString(m_command.readAllStandardOutput()).replace(QRegExp("[\\n\\r]"),"<br>"));
}
