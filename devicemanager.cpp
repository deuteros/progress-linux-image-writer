#include "devicemanager.h"

DeviceManager::DeviceManager()
{

    connect(&m_lsblk,SIGNAL(finished(int)),this,SLOT(onFinished(int)));
}

void DeviceManager::getRemovableDevices(){
    m_lsblk.start("lsblk", QStringList() << "-J" << "-d" << "-n" <<"-o" << "NAME,RO,RM,SIZE,MODEL,GROUP");
}


void DeviceManager::onFinished(int exitCode){
    QStringList devicesList;
    if (exitCode==0){
        QJsonDocument doc=QJsonDocument::fromJson(m_lsblk.readAll());

        if (doc.isObject()){
            QJsonObject obj = doc.object();
            if (obj.value("blockdevices").isArray()){
                QJsonArray devices= obj.value("blockdevices").toArray();
                for(int i=0; i<devices.count();i++){
                    //Make sure is a writable and removable disk
                    if (devices.at(i).toObject().value("ro").toString()=="0" && devices.at(i).toObject().value("rm").toString()=="1" && devices.at(i).toObject().value("group").toString()=="disk"){
                        QString name= devices.at(i).toObject().value("name").toString().toUtf8();
                        QString size= devices.at(i).toObject().value("size").toString().toUtf8();
                        QString model= devices.at(i).toObject().value("model").toString().toUtf8();
                        devicesList.append(name+" ("+size+" "+model+")");
                    }else{
                        qDebug() << "Skiping " << devices.at(i).toObject().value("name").toString().toUtf8();
                    }
                }
                emit devicesFound(devicesList);
            }else {
                qWarning() << "Malformed JSON response";
            }
        }
        else{
            qWarning() << "Unknown response format";
        }
    }else{
        qWarning() << "An error occurred: "+ m_lsblk.errorString();
        //emit errorMessage("An error occurred: "+ m_lsblk.errorString());
    }
}
