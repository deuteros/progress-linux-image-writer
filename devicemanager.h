#ifndef DEVICEMANAGER_H
#define DEVICEMANAGER_H

#include <QObject>
#include <QStorageInfo>
#include <QProcess>
#include <QString>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <qdebug.h>


class DeviceManager: public QObject
{
    Q_OBJECT
public:
    DeviceManager();
    Q_INVOKABLE void getRemovableDevices();

signals:
    void devicesFound(QStringList devices);

private slots:
    void onFinished(int exitCode);
private:
    QProcess m_lsblk;
};

#endif // DEVICEMANAGER_H
