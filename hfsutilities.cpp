#include "hfsutilities.h"

HFSUtilities::HFSUtilities(QObject *parent) : QObject(parent)
{
    connect(&m_hfs,SIGNAL(finished(int)),this,SLOT(onFinished(int)));
    connect(&m_hfs,&QProcess::errorOccurred,this,&HFSUtilities::onErrorOccurred);
    connect(&m_hfs,&QProcess::readyReadStandardError,this,&HFSUtilities::onReadyReadStandardError);
    connect(&m_hfs,&QProcess::readyReadStandardOutput,this,&HFSUtilities::onReadyReadStandardOutput);
}

void HFSUtilities::callHFSCommand(QString command, QStringList args){
    m_hfs.start(command, args);
}


void HFSUtilities::formatBootablePartition(QString device){
    QString partition("/dev/"+device+"2");
    emit message("<b>Formatting partition "+ partition + "</b");
    callHFSCommand("hformat",QStringList() << partition << "-l" << "boot");

}

void HFSUtilities::mount(QString device){
    QString partition("/dev/"+device+"2");
    emit message("<b>Mounting "+ partition + "</b");
    callHFSCommand("hmount",QStringList() << partition);

}

void HFSUtilities::umount(){
    emit message("<b>Disattaching partition</b");
    callHFSCommand("humount",QStringList());
}

void HFSUtilities::setType(){
    emit message("<b>Seting Yaboot attributes </b");
    callHFSCommand("hattrib",QStringList() <<"-c" << "UNIX" << "-t" << "tbxi" << ":yaboot");
}

void HFSUtilities::bless(){
    emit message("<b>Blessing... </b");
    callHFSCommand("hattrib",QStringList() << "-b" << ":");
}

void HFSUtilities::copyFile(QString file){
    emit message("<b>Copying " +QFileInfo(file).fileName() +" </b>");
    QFile::copy(file, QFileInfo(file).fileName());
    callHFSCommand("hcopy",QStringList() << "-r" << QFileInfo(file).fileName() << ":");
}


void HFSUtilities::onFinished(int exitCode){
    if (exitCode==0){
        QString data=m_hfs.readAll();
        emit message(QString("Done! <pre>"+data.replace(QRegExp("[\\n\\r]"),"<br>")+"</pre>"));
        emit finished();
    }else{
        emit errorMessage("An error occurred: Error code is" + QString::number(m_hfs.error()));
    }
}

void HFSUtilities::onErrorOccurred(QProcess::ProcessError err){
    emit errorMessage("An error occurred: Error code is " + QString::number(err));
}

void HFSUtilities::onReadyReadStandardError(){
    emit errorMessage(m_hfs.readAllStandardError());
}

void HFSUtilities::onReadyReadStandardOutput(){
    emit message(m_hfs.readAllStandardOutput());
}
