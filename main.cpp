#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include "devicemanager.h"
#include <partitioner.h>
#include <hfsutilities.h>
#include "fileinstaller.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQuickStyle::setStyle("Material");
    QQmlApplicationEngine engine;

    qmlRegisterType<DeviceManager>("DeviceManager", 1,0, "DeviceManager");
    qmlRegisterType<Partitioner>("Partitioner", 1,0, "Partitioner");
    qmlRegisterType<HFSUtilities>("HFSUtilities", 1,0, "HFSUtilities");
    qmlRegisterType<FileInstaller>("FileInstaller", 1,0, "FileInstaller");

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
