import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.0

import DeviceManager 1.0
import Partitioner 1.0
import HFSUtilities 1.0
import FileInstaller 1.0

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 640
    height: 480
    minimumHeight: 300
    minimumWidth: 500
    title: qsTr("PPC Image Writer")

    Material.theme: Material.System

    ColumnLayout {
        id: column
        spacing: 5
        anchors.fill: parent
        anchors.margins: 20

        GridLayout {
            id: grid
            width: parent.width
            height: 170
            rows: 3
            columns: 2
            columnSpacing: 10
            rowSpacing: columnSpacing

            Label {
                id: lblDevice
                text: qsTr("Device")
            }

            RowLayout{
                ComboBox {
                    id: cbDevices
                    property string device
                    function getDeviceName(){
                        var words= currentText.split(" ")
                        device=words[0]
                        return device
                    }
                    onCurrentTextChanged: getDeviceName()
                    Layout.fillWidth: true

                    Component.onCompleted: deviceManager.getRemovableDevices()
                }
                Button{
                    text: "Reload"
                    onClicked: deviceManager.getRemovableDevices()

                }
            }

            Label {
                id: lblRootFS
                text: qsTr("Root File System")
            }

            RowLayout{
                TextField{id: tiRootFS; Layout.fillWidth: true; clip: true; onTextChanged: btnStart.tryToEnable()}
                Button {
                    id: btnRootFS
                    text: qsTr("...")
                    onClicked: rootFSDialog.open()

                    FileDialog {
                        id: rootFSDialog
                        title: "Please choose a file"
                        width:300
                        height:300
                        nameFilters: [ "tar.bz2 (*.tar*.bz2)", "tar.xz files (*.tar*.xz)","tar.gz files (*.tar*.gz)","All files (*)" ]

                        onRejected: {
                            visible = false
                        }
                        onAccepted: tiRootFS.text=fileUrl

                    }
                }
            }
        }

        Item{
            height: 20
            Layout.fillHeight: true

        }

        Row {
            id: row
            height: 100
            width: 200
            spacing: 5
            anchors.right: parent.right

            Button {
                id: btnStart
                function tryToEnable(){
                    enabled = cbDevices.count>0 && tiRootFS.text.length>0
                }

                text: qsTr("Start")
                onClicked: {
                    dlgConfirmOperation.open()
                }
            }

            Button {
                id: btnCancel
                text: qsTr("Cancel")
                onClicked: mainWindow.close()
            }
        }
    }

    ConfirmOperation{
        id: dlgConfirmOperation

        onAccepted: {
            mainWindow.hide()
            winProgress.append("<b>"+cbDevices.device+" partitioning started...</b>")
            winProgress.show()
            imageCreatorScript.start()

        }

        Component.onCompleted: {
            x=mainWindow.x +mainWindow.width/2-width/2
            y=mainWindow.y+ mainWindow.height/2-height/2
        }
    }


    WindowProgress{
        id: winProgress
        visible: false
        x:mainWindow.x
        y:mainWindow.y

    }

    DeviceManager{
        id:deviceManager
        onDevicesFound: {
            console.log(devices)
            cbDevices.model=devices
            btnStart.tryToEnable()
        }
    }

    Partitioner{
        id: partitioner

        onMessage: winProgress.append(msg)
        onErrorMessage: winProgress.append(msg)
    }

    HFSUtilities{
        id: hfs
        onMessage: winProgress.append(msg)
        onErrorMessage: winProgress.append(msg)

    }

    FileInstaller{
        id: fileInstaller

        onMessage: winProgress.append(msg)
        onErrorMessage: winProgress.append(msg)
    }

    Item {
        id: imageCreatorScript
        property int count:0

        function start(){
            count=0
            partitioner.makeLabel(cbDevices.device)
        }

        signal finished()

        Connections{
            target: partitioner
            onFinished:{
                if (imageCreatorScript.count==0){
                    partitioner.makeBootPartition(cbDevices.device)
                }else if (imageCreatorScript.count==1){
                    partitioner.makePartitionBootable(cbDevices.device)
                }else if (imageCreatorScript.count==2){
                    partitioner.makePrimaryPartition(cbDevices.device)
                }else if (imageCreatorScript.count==3){
                    partitioner.print(cbDevices.device)
                }else if (imageCreatorScript.count==4){
                    hfs.formatBootablePartition(cbDevices.device)
                }

                imageCreatorScript.count++;
            }
            onErrorMessage: {
                imageCreatorScript.finished()
            }
        }

        Connections{
            target:hfs
            onFinished: {
                if (imageCreatorScript.count==5){
                    hfs.mount(cbDevices.device)
                }else if (imageCreatorScript.count==6){
                    hfs.copyFile(":/data/yaboot")
                }else if (imageCreatorScript.count==7){
                    hfs.copyFile(":/data/yaboot.conf")
                }else if (imageCreatorScript.count==8){
                    hfs.copyFile(":/data/boot.msg")
                }else if (imageCreatorScript.count==9){
                    hfs.bless()
                }else if (imageCreatorScript.count==10){
                    hfs.umount()
                }else if (imageCreatorScript.count==11){
                    fileInstaller.mkfsext3(cbDevices.device)
                }
                imageCreatorScript.count++
            }
            onErrorMessage: {
                imageCreatorScript.finished()
            }
        }

        Connections{
            target: fileInstaller
            onFinished: {
                if (imageCreatorScript.count==12){
                    fileInstaller.mount(cbDevices.device)
                }/*else if (imageCreatorScript.count==13){
                    fileInstaller.copyFile(tiRootFS.text,"/mnt")
                }else if (imageCreatorScript.count==14){
                    fileInstaller.cd("/mnt")
                }*/else if (imageCreatorScript.count==13){
                    fileInstaller.extract(tiRootFS.text,"/mnt")
                }else if (imageCreatorScript.count==14){
                    fileInstaller.sync()
                }else if (imageCreatorScript.count==15){
                    fileInstaller.umount(cbDevices.device)
                    imageCreatorScript.count=-1
                    imageCreatorScript.finished()
                }
                imageCreatorScript.count++
            }

            onErrorMessage: {
                imageCreatorScript.finished()
            }

        }
    }
}
