%define breq libqt5-qtbase-devel  libQt5QuickControls2-devel  
%define reqs libqt5-qtbase parted libqt5-qtquickcontrols2 libQt5QuickControls2
%define qmake /usr/bin/qmake  
%define lrelease /usr/bin/lrelease  
%define _qt5_translationdir /usr/share/qt5/translations  

Name: ppciw
Summary: Power Progress Community Image Writer
Version: 0.1.0
Release: 1
License: GPL v3
Group: Productivity/Networking/File-Sharing 
BuildRoot: %{_tmppath}/build-root-%{name}
Source0: /home/abuild/rpmbuild/SOURCES/progress-linux-image-writer-master.tar.gz
Packager: Guillermo Amat
Url: https://www.powerprogress.org
Vendor: Power Progress Community

BuildRequires: gcc-c++, make, %{breq} 
Requires: %{reqs} mercurial >= 1.7 libc.so.6 libgcc_s.so.1 libgcc_s.so.1(GCC_3.0) libm.so.6 libpthread.so.0 libstdc++.so.6 libstdc++.so.6(CXXABI_1.3) libstdc++.so.6(GLIBCXX_3.4)       
%description
Short description

Long description

%prep
%setup -q -n progress-linux-image-writer-master
cat > %{name}.desktop << EOF
[Desktop Entry]
Encoding=UTF-8
Name=%{name}
GenericName=%{name}
GenericName[de]=%{name}
Comment=Creates a bootable USB drive for Power Macs
Exec=%{name}
Icon=%{name}
Terminal=false
Type=Application
StartupNotify=false
Categories=System;Utility;Archiving;
MimeType=application/x-cd-image;application/x-raw-disk-image;
EOF

%build
qmake-qt5 
CFLAGS="$RPM_OPT_FLAGS" CXXFLAGS="$RPM_OPT_FLAGS" \
make %{?_smp_mflags}
#%{lrelease} %{name}.pro

%install
#make %{?_smp_mflags} INSTALL_ROOT=%{buildroot} install
install -D -m 0755 %{name} %{buildroot}%{_bindir}/%{name}
install -D -m 0755 %{name}.desktop %{buildroot}%{_datadir}/applications/%{name}.desktop
install -D -m 0755 %{_sourcedir}/%{name}.png %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/%{name}.png

%clean
rm -rf %{buildroot}

%files  
%defattr(-,root,root)  
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor
%{_datadir}/icons/hicolor/64x64
%{_datadir}/icons/hicolor/64x64/apps
%{_datadir}/icons/hicolor/*/apps/%{name}.png

%changelog
