import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 2.2

Dialog {

    title: "Confirm operation"

    standardButtons: StandardButton.Ok | StandardButton.Cancel

    Label {
        id: label
        text: qsTr("Warning this will wipe your device. Do you really want to continue?")
    }
}
