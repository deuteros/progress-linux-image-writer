import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.0


Window {
    id: root
    function append(msg){
        txtMsg.append(msg)
    }


    visible: true
    width: 640
    height: 500
    minimumHeight: 500
    minimumWidth: 500
    title: qsTr("Creating image")

    ColumnLayout {
        id: column
        spacing: 5
        anchors.fill: parent
        anchors.margins: 20

        ScrollView {
            id: view
            width: parent.width
            ScrollBar.vertical.policy: ScrollBar.AlwaysOn
            padding: 20

            Layout.fillHeight: true
            Layout.fillWidth: true

            TextArea{
                id: txtMsg
                wrapMode: Text.WordWrap
                textFormat: Text.RichText
            }
        }

        RowLayout {
            id: row
            height: 100
            width: 200
            spacing: 5
            anchors.right: parent.right

            Item{
                Layout.fillWidth: true
            }

            Button {
                id: btnFinish
                enabled: false
                text: qsTr("Close")
                onClicked: {
                    mainWindow.show()
                    txtMsg.clear()
                    enabled=false
                    root.close()
                }

                Connections{
                    target: imageCreatorScript
                    onFinished: btnFinish.enabled=true

                }
            }

        }
    }
}
