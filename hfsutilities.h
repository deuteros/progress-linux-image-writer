#ifndef HFSUTILITIES_H
#define HFSUTILITIES_H

#include <QObject>
#include <QProcess>
#include <QFile>
#include <QFileInfo>
#include <QtDebug>

class HFSUtilities : public QObject
{
    Q_OBJECT
public:
    explicit HFSUtilities(QObject *parent = nullptr);
    Q_INVOKABLE void callHFSCommand(QString command, QStringList args);
    Q_INVOKABLE void formatBootablePartition(QString device);
    Q_INVOKABLE void mount(QString device);
    Q_INVOKABLE void umount();
    Q_INVOKABLE void setType();
    Q_INVOKABLE void bless();
    Q_INVOKABLE void copyFile(QString sourceFile);

private:
    QProcess m_hfs;

signals:
    void message(QString msg);
    void errorMessage(QString msg);
    void finished();

public slots:
    void onFinished(int exitCode);
    void onErrorOccurred(QProcess::ProcessError error);
    void onReadyReadStandardOutput();
    void onReadyReadStandardError();
};

#endif // HFSUTILITIES_H
