﻿#include "partitioner.h"

Partitioner::Partitioner(QObject *parent) : QObject(parent)
{
    connect(&m_parted,SIGNAL(finished(int)),this,SLOT(onFinished(int)));
    connect(&m_parted,&QProcess::errorOccurred,this,&Partitioner::onErrorOccurred);
    connect(&m_parted,&QProcess::readyReadStandardError,this,&Partitioner::onReadyReadStandardError);
    connect(&m_parted,&QProcess::readyReadStandardOutput,this,&Partitioner::onReadyReadStandardOutput);
}

void Partitioner::callPartedCommand( QStringList args){
    m_parted.start("parted", args);

}

void Partitioner::makeLabel(QString device){
    emit message("<b>Creating partition table</b>");
    callPartedCommand(QStringList() << "--script" << QString("/dev/"+device) << "mklabel" << "mac");

}

void Partitioner::makeBootPartition(QString device){
    emit message("<b>Creating boot partition</b>");
    callPartedCommand(QStringList() << "--script" << "-a" << "cyl" << QString("/dev/"+device) << "mkpart" <<  "boot" << "hfs" << "33.8kB" << "32MB");
}

void Partitioner::makePartitionBootable(QString device){
    emit message("<b>Making partition bootable</b>");
    callPartedCommand(QStringList() << "--script" << QString("/dev/"+device) << "set" << "2" << "boot" << "on");
}


void Partitioner::makePrimaryPartition(QString device){
    emit message("<b>Creating primary partition</b>");
    callPartedCommand( QStringList() << "--script" << "-a" << "cyl" << QString("/dev/"+device) << "mkpart" << "primary" << "ext3"<< "32MB"  << "100%" );
    emit message("Primary partition created ");
}

void Partitioner::print(QString device){
    emit message("<b>Listing disk partitions</b>");
    callPartedCommand( QStringList() << "--script" <<  QString("/dev/"+device) << "print");
}

void Partitioner::onFinished(int exitCode){
    if (exitCode==0){
        QString data=m_parted.readAll();
        emit message(QString("Done!<br><pre>"+data.replace(QRegExp("[\\n\\r]"),"<br>")+"</pre>"));
        emit finished();
    }else{
        emit errorMessage("An error occurred: Error code is" + QString::number(m_parted.error()));
    }
}

void Partitioner::onErrorOccurred(QProcess::ProcessError err){
    emit errorMessage("An error occurred: Error code is " + QString::number(err));
}

void Partitioner::onReadyReadStandardError(){
    emit errorMessage(QString(m_parted.readAllStandardError()).replace(QRegExp("[\\n\\r]"),"<br>"));
}

void Partitioner::onReadyReadStandardOutput(){
    emit message(QString(m_parted.readAllStandardOutput()).replace(QRegExp("[\\n\\r]"),"<br>"));
}
