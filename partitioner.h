#ifndef PARTITIONER_H
#define PARTITIONER_H

#include <QObject>
#include <QProcess>
#include <QtDebug>

class Partitioner: public QObject
{
    Q_OBJECT
public:
    explicit Partitioner(QObject *parent = nullptr);
    Q_INVOKABLE void makeLabel(QString device);
    Q_INVOKABLE void makeBootPartition(QString device);
    Q_INVOKABLE void makePartitionBootable(QString device);
    Q_INVOKABLE void makePrimaryPartition(QString device);
    Q_INVOKABLE void print(QString device);

private:
    void callPartedCommand(QStringList args);

private slots:
    void onFinished(int exitCode);
    void onErrorOccurred(QProcess::ProcessError error);
    void onReadyReadStandardOutput();
    void onReadyReadStandardError();

signals:
    void message(QString msg);
    void errorMessage(QString msg);
    void finished();

private:
    QProcess m_parted;
};

#endif // PARTITIONER_H
