#ifndef FILEINSTALLER_H
#define FILEINSTALLER_H

#include <QObject>
#include <QProcess>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QtDebug>
#include <QUrl>

class FileInstaller : public QObject
{
    Q_OBJECT
public:
    explicit FileInstaller(QObject *parent = nullptr);
    void callCommand(QString command, QStringList args);
    Q_INVOKABLE void mkfsext3(QString device);
    Q_INVOKABLE void mount(QString device);
    Q_INVOKABLE void umount(QString device);
    Q_INVOKABLE void sync();
    Q_INVOKABLE void extract(QString file, QString targetDir="");
    Q_INVOKABLE void cd(QString dir);
    Q_INVOKABLE void copyFile(QString sourceFile, QString targetFile);

private:
    QProcess m_command;

signals:
    void message(QString msg);
    void errorMessage(QString msg);
    void finished();

public slots:
    void onFinished(int exitCode);
    void onErrorOccurred(QProcess::ProcessError error);
    void onReadyReadStandardOutput();
    void onReadyReadStandardError();
};

#endif // FILEINSTALLER_H
